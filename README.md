# Pellicciotto--Server
Goals (by priority):
- data models for TURF;
- import Aspiturf data;
- provide data and run statistical models;
- provide web UI and REST API in order to generate infos;
- monitor external sources in order to update turf data in realtime;


# Architecture
Project is based on Django, it provides its own data model in order to optimize
general throughput. Client side assets are generated using Webpack, using Vue framework.

Data source models should be implemented in different application in order to
keep shit clean. Thoses application provide importer to pellicciotto data models.

## App: instance
Django project settings

## App: pellicciotto
This Django app provides models and tools to handle TURF data:

Backend:
- base models:
    - **base**: provide common information and summary stats | *Track, Horse, Human*;
    - **race**: information related to a race | *Race, RaceRunner, RaceCouple, RaceRunner*;
- worker-pool: async tasks pool (separated threads)
    - for: run stats model, fetch data from external sources;
    - must avoid to run multiple time the same time;
- import: base class in order to import data from external sources
- app: loader for sources and importers

Frontend:
- Vue + Webpack
- base templates and assets
- task poll

## App: Aspiturf
[TODO] Provides models and importer for Aspiturf database.


# Getting started
Requirements: python3, virtualenv, npm, webpack.


# Getting started
## Server installation
```sh
# Create virtual environment
virtualenv venv
# Activate virtual environment
source venv/bin/activate
# Install dependencies
pip install -r requirements.txt
```

Edit Django project settings in `instance/settings.py`.
Database (when using Mysql or Mariadb): `db.cnf`.

Create and update database tables:
```sh
source venv/bin/activate
# Create or update migration files
./manage.py makemigrations
# Migrate
./manage.py migrate
```

## Assets installation:
Install dependencies:
```sh
npm install
```

Generate assets (production):
```sh
npx webpack --mode production
```

Generate assets (development):
```sh
npx webpack --mode development
# or run with `--watch` in order to automatically generate on file
# change
npx webpack --mode development --watch
```

## Running
Run server:
```sh
source venv/bin/activate
./manage.py runserver "http://127.0.0.1:8000/"
```

From web browser, you can access to the interface through `http://127.0.0.1:8000`.
Admin interface: `http://127.0.0.1:8000/admin`

