import components from './components'
import Model from './models'

const App = {
    el: '#app',
    delimiters: ['[[', ']]'],
    components: {...components},

    data() {
        return {
            Model,
        }
    }
}

export default App


