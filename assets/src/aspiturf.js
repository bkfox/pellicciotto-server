import './index.js'

import { nextTick } from 'vue';
import App from './app'
import List from './list'
import { CacheDate, ImportStatus }  from './models'


export const Aspiturf = {
    ...App,

    props: {
        ...(App.props || {}),
        filtersForm: {type: String, default: '#filters-form'},
        listUrl: String,
        importStatusUrl: String,
        importStatusTimeout: { type: Number, default: 5000 },
        runImportUrl: String,
    },

    data() {
        return {
            list: this.listUrl && new List(CacheDate, this.listUrl),
            importStatus: null,
        }
    },

    computed: {
        items() {
            return this.list && this.list.items
        },
        
        tasks() {
            if(!this.importStatus)
                return {}

            const data = this.importStatus.data
            if(!data || !data.tasks)
                return {}

            let futures = {}
            for(let key in data.tasks) {
                const task = data.tasks[key]
                if(!task)
                    continue

                for(let fut of task.futures) {
                    const future = futures[fut[0]] || [0,0]
                    if(fut[1])
                        future[0] += 1
                    future[1] += 1
                    futures[fut[0]] = future
                }
            }
            return futures
        }
    },

    methods: {
        ...(App.methods || {}),

        fetchImportStatus(timeout=null) {
            // FIXME: ecoInterval?
            if(timeout)
                setTimeout(() => this.fetchImportStatus(timeout), timeout)
            return ImportStatus.fetch(this.importStatusUrl)
                               .then(r => { this.importStatus = r })
        },

        runImport() {
            const postData = new FormData(document.querySelector(this.filtersForm))
            return ImportStatus.fetch(this.runImportUrl, { method: 'POST', body: postData })
                               .then(r => { this.importStatus = r })
        },
    },

    mounted() {
        nextTick(() => {
            this.list && this.list.fetch()
            this.importStatusUrl && this.fetchImportStatus(this.importStatusTimeout)
        })
    },
}

window.Aspiturf = Aspiturf

export default Aspiturf

