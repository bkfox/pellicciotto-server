/**
 * Run function with provided args only if document is not hidden
 */
export function setEcoTimeout(func, ...args) {
    return setTimeout((...args) => {
        !document.hidden && func(...args)
    }, ...args)
}

/**
 * Run function at specific interval only if document is not hidden
 */
export function setEcoInterval(func, ...args) {
    return setInterval((...args) => {
        !document.hidden && func(...args)
    }, ...args)
}

