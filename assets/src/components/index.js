import AAutocomplete from './AAutocomplete.vue'
import AList from './AList.vue'

/**
 * Core components
 */
export default {
    AAutocomplete, AList,
}

