import { createApp } from 'vue'
import '@fortawesome/fontawesome-free/css/all.min.css'
import '@fortawesome/fontawesome-free/css/fontawesome.min.css'
import './assets/styles.scss'


window.createApp = function(config, {el='#app', ...props}={}) {
    const app = createApp(config, props)
    const vm = app.mount(el)
    window.app = app
    window.vm = vm
    return [app, vm]
}


