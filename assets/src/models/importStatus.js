import Model from './model'


export default class ImportStatus extends Model {
    /**
     * Return tasks as Object by future key of `[done, total]`.
     */
    getTasks() {
        if(!this.data || !this.data.tasks)
            return {}

        let futures = {}
        for(let key in this.data.tasks) {
            const task = this.data.tasks[key]
            if(!task)
                continue

            for(let fut of task.futures) {
                const future = futures[fut[0]] || [0,0]
                if(fut[1])
                    future[0] += 1
                future[1] += 1
                futures[fut[0]] = future
            }
        }
        return futures
    }
}


