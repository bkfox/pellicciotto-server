
/**
 * Return cookie with provided key
 */
function getCookie(key) {
    if(document.cookie && document.cookie !== '') {
        const cookie = document.cookie.split(';')
                               .find(c => c.trim().startsWith(key + '='))
        return cookie ? decodeURIComponent(cookie.split('=')[1]) : null;
    }
    return null;
}

/**
 * CSRF token provided by Django
 */
var csrfToken = null;

/**
 * Get CSRF token
 */
export function getCsrf() {
    if(csrfToken === null)
        csrfToken = getCookie('csrftoken')
    return csrfToken;
}


/**
 * Provide interface used to fetch and manipulate objects.
 */
export default class Model {
    /**
     * Instanciate model with provided data and options.
     * By default `url` is taken from `data.url_`.
     */
    constructor(data, {url=null, ...options}={}) {
        this.url = url || data.url_;
        this.options = options;
        this.commit(data);
    }

    /**
     * Get instance id from its data
     */
    static getId(data) {
        return data.id;
    }

    /**
     * Return fetch options
     */
    static getOptions(options) {
        return {
            ...options,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'X-CSRFToken': getCsrf(),
                ...(options.headers || {}),
            },
        }
    }

    /**
     * Return model instances for the provided list of model data.
     * @param {Array} items: array of data
     * @param {Object} options: options passed down to all model instances
     */
    static fromList(items, options={}) {
        return items ? items.map(d => new this(d, options)) : []
    }

    /**
     * Fetch item(s) from server
     */
    static fetch(url, {many=false, filtersForm=null, params=null, ...options}={}, args={}) {
        options = this.getOptions(options)
        const request = fetch(url, options).then(response => response.json());

        if(!params)
            params = new URLSearchParams();

        if(filtersForm) {
            if(typeof filtersForm == 'string')
                filtersForm = document.querySelector(filtersForm)
            if(filtersForm)
                for(const item of Array.from(filtersForm.elements))
                    params.set(item.name, item.value)
        }

        const paramsString = params.toString()
        if(paramsString)
            url += '?' + params.toString()
        
        if(many)
            return request.then(data => {
                const dataList = (data instanceof Array) ? data : data.results
                return {items: this.fromList(dataList, args), data: data}
            })
        else
            return request.then(data => new this(data, {url: url, ...args}));
    }

    /**
     * Fetch data from server.
     */
    fetch(options) {
        options = this.constructor.getOptions(options)
        return fetch(this.url, options)
            .then(response => response.json())
            .then(data => this.commit(data));
    }

    /**
     * Call API action on object.
     */
    action(path, options, commit=false) {
        options = this.constructor.getOptions(options)
        const promise = fetch(this.url + path, options);
        return commit ? promise.then(data => data.json())
                               .then(data => { this.commit(data); this.data })
                      : promise;
    }

    /**
     * Update instance's data with provided data. Return None
     */
    commit(data) {
        this.id = this.constructor.getId(data);
        this.data = data;
    }

    /**
     * Save instance into localStorage.
     */
    store(key) {
        window.localStorage.setItem(key, JSON.stringify(this.data));
    }

    /**
     * Load model instance from localStorage.
     */
    static storeLoad(key) {
        let item = window.localStorage.getItem(key);
        return item === null ? item : new this(JSON.parse(item));
    }
}


