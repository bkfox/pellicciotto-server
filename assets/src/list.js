
export default class List {
    nextUrl = null;
    isLoading = false;
    totalCount = false;
    
    constructor(model, url, {items=[],filtersForm=null}={}) {
        this.model = model
        this.url = url
        this.items = items
        this.filtersForm = filtersForm
    }

    fetch({url=null,append=false,...options}={}) {
        if(this.isLoading)
            return
        url = url || this.url
        options.many = true
        options.filtersForm = options.filtersForm || this.filtersForm
        this.isLoading = true
        return this.model.fetch(url, options).then(result => {
            this.isLoading = false;
            this.nextUrl = result.data.next;
            this.url = url;
            this.totalCount = result.data.count;
            if(append)
                this.items.splice(this.items.length, 0, ...result.items)
            else {
                this.items.splice(0, this.items.length, ...result.items)
            }
            return result
        })
    }

    find(pred) { return this.items.find(pred) }
    findIndex(pred) { return this.items.findIndex(pred) }
}


