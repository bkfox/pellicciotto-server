const path = require('path');

const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
    transpileDependencies: true,
    outputDir: path.resolve('../pellicciotto/static/pellicciotto/'),
    publicPath: './',
    runtimeCompiler: true,
    filenameHashing: false,
    
    css: {
        extract: true,
        loaderOptions: {
            sass: { sourceMap: true },
        }
    },

    pages: {
        core: {entry: 'src/core.js'},
        aspiturf: {entry: 'src/aspiturf.js'},
    }
})
