from django.urls import path, include
from rest_framework.routers import DefaultRouter

from . import views, viewsets

__all__ = ('urls', 'router')

router = DefaultRouter()
router.register('track', viewsets.TrackViewSet, 'track')
router.register('human', viewsets.HumanViewSet, 'human')
router.register('horse', viewsets.HorseViewSet, 'horse')
router.register('race', viewsets.RaceViewSet, 'race')
router.register('race_runner', viewsets.RunnerViewSet, 'race_runner')


urls = [
#    path('', views.AppView.as_view(), name='aspiturf-app'),
    path('api/', include(router.urls)),
]


