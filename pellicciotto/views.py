from django.views.generic.base import View as BView, TemplateResponseMixin
from . import models

__all__ = ('View',)

class View(TemplateResponseMixin, BView):
    model = None

    def get_context_data(self, **kwargs):
        kwargs['model'] = self.model
        return kwargs

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


