from rest_framework import serializers
from tools.serializers import MapField, IntegerField
from ..models import Human, Horse, Race, Runner, Weather


class NamedSerializer(serializers.Serializer):
    ID = IntegerField()
    name = serializers.CharField()
    
class HumanSerializer(serializers.ModelSerializer):
    ID = IntegerField(source='source_id')
    name = serializers.CharField()

    class Meta:
        model = Human
        fields = ('ID', 'name')

class WeatherSerializer(serializers.ModelSerializer):
    """
    Units:
        - clouds: integer at least from 0 to 100 (=8 on equidia/pmu)
        - wind: kth
    """
    clouds = IntegerField(source='clouds_bing')
    icon = serializers.CharField()
    windDeg = IntegerField(source='wind_degree')
    windDir = IntegerField(source='wind_direction')
    lastUpdate = serializers.DateTimeField(source='update_date')
    
    class Meta:
        model = Weather
        fields = ('temperature', 'wind', 'rain', 'lightning',
                  'clouds', 'icon', 'windDeg', 'windDir', 'lastUpdate')

class RaceSerializer(serializers.ModelSerializer):
    type = NamedSerializer()
    weather = WeatherSerializer()

    # track
    pmuFrEvent = IntegerField(source='pmu_meeting')
    nr = IntegerField(source='number')
    result = serializers.CharField()

    class Meta:
        model = Race
        fields = ('type', 'weather',
                  'pmuFrEvent', 'nr', 'result',
                  'name', 'date', 'cancelled', 'jumping', 'distance', 'stake')

class OddSerializer(serializers.Serializer):
    provider = serializers.CharField()
    odd = serializers.FloatField()
    oddDate = serializers.DateTimeField()

class RunnerSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='horse.name')
    sex = serializers.MapField(source='horse.sex', map={
        Horse.SEX_FEMALE: 'F', Horse.SEX_MALE: 'M', Horse.SEX_GELDING: 'H'
    })
    age = IntegerField(source='horse.age')
    owner = HumanSerializer('horse.owner')

    jockey = HumanSerializer('jockey')
    trainer = HumanSerializer('trainer')
    nr = IntegerField(source='number')
    nv = serializers.BooleanField(source='non_runner')
    pos = IntegerField(source='position')

    shoe = MapField(default=None, map={
        None: 'NONE', Runner.SHOE_BACK: 'HIND', Runner.SHOE_FRONT_BACK: 'BOTH',
    }, keep_unsafe=True)
    blinder = MapField(default=None, map={
        None: 'NONE', Runner.BLINDER_SOME: 'BLINDERS',
        Runner.BLINDER_AUSTRALIAN: 'WINKERS',
    }, keep_unsafe=True)
    weight = IntegerField(required=False)

    odds = OddSerializer(many=True)
    yearEarnings = IntegerField(source='year_earnings', required=False)
    prevYearEarnings = IntegerField(source='prev_year_earnings', required=False)
    victoriesTotalCashAmount = IntegerField(source='total_victories_earnings', required=False)
    record = IntegerField(required=False)

    class Meta:
        model = Runner
        fields = (
                'name', 'sex', 'age', 'owner', 
                'jockey', 'trainer', 'team',
                'nr', 'rail', 'nv', 'pos', 'shoe', 'blinder', 'weight',
                'gains', 'odds', 'yearEarnings', 'prevYearEarnings', 'victoriesTotalCashAmount',
        )


