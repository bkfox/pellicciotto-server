""" PMU.fr API's serializers. """
from rest_framework import serializers

from tools.serializers import MapField
from .. import models

# Get horses of a run
# https://online.turfinfo.api.pmu.fr/rest/client/61/programme/13042022/R4/C8/participants?specialisation=INTERNET

class TrackSerializer(serializers.ModelSerializer):
    code = serializers.CharField(source='code')
    libelleCourt = serializers.CharField(source='name')
    libelleLong = serializers.CharField(source='long_name')
    
    class Meta:
        model = models.Track
        fields = ['code', 'libelleCourt', 'libelleLong']


class ProfitsSerializer(serializers.Serializer):
    gainsCarriere = serializers.IntegerField(source='profits_career')
    gainsVictoires = serializers.IntegerField(source='profits_wins')
    gainsPlace = serializers.IntegerField(source='profits_place')
    gainsAnneeEnCours = serializers.IntegerField(source='profits_current_year')
    gainsAnneePreced = serializers.IntegerField(source='profits_last_year')


class HorseSerializer(serializers.ModelSerializer):
    # TODO:
    # - fields: race, stable
    # - eleveur etc. -> remove "M." "Mlle", etc.
    nom = models.CharField(source='nom')
    nomPere = models.CharField(source='father')
    nomMere = models.CharField(source='mother')
    sexe = MapField(source='sex', default=models.Horse.SEX_UNKNOWN, map={
        'FEMELLES': models.Horse.SEX_FEMALE, 'MALES': models.Horse.SEX_MALE,
        'HONGRES': models.Horse.SEX_GELDING
    })
    age = models.IntegerField(min_value=1)
    musique = models.CharField(source='rank')
    jumentPleine = models.BooleanField(source='pregnant')
    nombreCourses =  models.IntegerField(source='races')
    nombreVictoires =  models.IntegerField(source='wins')
    nombrePlaces =  models.IntegerField(source='places')
    nombrePlacesSecond =  models.IntegerField(source='places_second')
    nombrePlacesTroisieme =  models.IntegerField(source='places_third')
    gainsParticipant = ProfitsSerializer(source='*')

    class Meta:
        model = models.Horse

class RunnerSerializer(serializers.ModelSerializer):
    numPmu = serializers.IntegerField(source='number')
    # arrival_outdistance
    # cote_pmu, cote_prob, gains
    # FIXME: check values
    oeilleres = MapField(source='blinder', default=models.Runner.BLINDER_NONE, map={
        models.Horse.BLINDER_NONE: 'SANS_OEILLERES', models.Horse.BLINDER_SOME: 'AVEC_OEILLERES',
        models.Horse.BLINDER_AUSTRALIAN: 'OEILLERES_AUSTRALIENNES'
    }, required=False)
    deferre = MapField(source='shoeing', default=models.Runner.SHOE_NONE, map={
        models.Runner.SHOE_BACK: 'DEFERRE_ANTERIEURS',
        models.Runner.SHOE_BACK: 'DEFERRE_POSTERIEURS',
        models.Runner.SHOE_FRONT_BACK: 'DEFERRE_ANTERIEURS_POSTERIEURS',
    }, required=False)

    indicateurInedit = serializers.BooleanField(source='first_race', default=False)
    handicapPoids = serializers.IntegerField(source='handicap_weight', required=False)
    handicapDistance = serializers.IntegerField(source='handicap_distance', required=False)

    supplement = serializers.IntegerField(default=0)
    engagement = serializers.IntegerField(default=0)
    

    class Meta:
        model = models.Runner

"""
XHRGEThttps://online.turfinfo.api.pmu.fr/rest/client/61/programme/13042022/R4/C8/participants?specialisation=INTERNET
[HTTP/2 200 OK 46ms]

    
    participants[ {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, … ]
    0Object { nom: "ELIEDEN", numPmu: 1, age: 6, … }
    race"PUR-SANG"
    statut"PARTANT"
    placeCorde7
    proprietaire"MLLE G.KELLEWAY/A. BARBER"
    entraineur"MLLE AS.CROMBEZ"
    driver"S.PASQUIER"
    driverChangefalse
    robeObject { code: "020", libelleCourt: "BAI", libelleLong: "BAI" }
    handicapValeur23.5
    nomPereMere"INVINCIBLE SPIRIT"
    engagementfalse
    poidsConditionMonteChangefalse
    dernierRapportDirectObject { typePari: "E_SIMPLE_GAGNANT", rapport: 14, typeRapport: "DIRECT", … }
    typePari"E_SIMPLE_GAGNANT"
    rapport14
    typeRapport"DIRECT"
    indicateurTendance"+"
    nombreIndicateurTendance7.69
    dateRapport1649843108000
    permutation1
    favorisfalse
    grossePrisefalse
    dernierRapportReferenceObject { typePari: "E_SIMPLE_GAGNANT", rapport: 13, typeRapport: "REFERENCE", … }
    urlCasaque"https://www.pmu.fr/back-assets/hippique/casaques/13042022/R4/C8/P1.png"
    eleveur"MME E. RYAN"
    paysEntrainement"FRA"
    allure"GALOP"
    1Object { nom: "O GRE DES SAISONS", numPmu: 2, age: 4, … }
    2Object { nom: "LANDARYNA", numPmu: 3, age: 4, … }
    3Object { nom: "PRINCE KERALI", numPmu: 4, age: 6, … }
    4Object { nom: "WHAT DO YOU SAY", numPmu: 5, age: 6, … }
    5Object { nom: "ROCK CHOP", numPmu: 6, age: 5, … }
    6Object { nom: "MYBOYGILLES", numPmu: 7, age: 6, … }
    7Object { nom: "KENBAIO", numPmu: 8, age: 6, … }
    8Object { nom: "MAKILEN SENORA", numPmu: 9, age: 4, … }
    9Object { nom: "BAY VIEW", numPmu: 10, age: 5, … }
    10Object { nom: "DUKE OF ARABIA", numPmu: 11, age: 5, … }
    11Object { nom: "COIGNY", numPmu: 12, age: 6, … }
    12Object { nom: "SALERNO", numPmu: 13, age: 9, … }
    13Object { nom: "CARRY OUT", numPmu: 14, age: 10, … }
    14Object { nom: "DEWI SANT", numPmu: 15, age: 4, … }
    15Object { nom: "WAYNE", numPmu: 16, age: 8, … }
    16Object { nom: "POTIOKA", numPmu: 17, age: 7, … }
    ecuries[]
    spriteCasaques[ {…}, {…}, {…} ]
    cachedtrue
"""

