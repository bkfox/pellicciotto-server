from rest_framework.serializers import ModelSerializer
from .. import models


__all__ = ('RaceSerializer', 'RunnerSerializer',)


class RaceSerializer(ModelSerializer):
    class Meta:
        model = models.Race
        fields = '__all__'

class RunnerSerializer(ModelSerializer):
    class Meta:
        model = models.Runner
        fields = '__all__'

