from rest_framework.serializers import ModelSerializer
from .. import models


__all__ = ('TrackSerializer', 'HumanSerializer', 'HorseSerializer')


class TrackSerializer(ModelSerializer):
    class Meta:
        model = models.Track
        fields = '__all__'

class HumanSerializer(ModelSerializer):
    class Meta:
        model = models.Human
        fields = '__all__'

class HorseSerializer(ModelSerializer):
    class Meta:
        model = models.Horse
        fields = '__all__'

