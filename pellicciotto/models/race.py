from django.db import models
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _

from .base import *
from ..units import length_to_float


__all__ = ('Race', 'Weather', 'Runner',)


class Race(models.Model):
    TYPE_FLAT = 0x00
    TYPE_HARNESS = 0x03 # confirmed
    TYPE_FENCE = 0x01
    TYPE_BRED = 0x02
    TYPE_STEEPLE_CHASE = 0x04
    TYPE_STEEPLE_CHASE_CROSS = 0x05
    TYPE_CHOICES = (
        (TYPE_HARNESS, _('Harness')),
        (TYPE_FENCE, _('Fence')),
        (TYPE_BRED, _('Bred')),
        (TYPE_FLAT, _('Flat')),
        (TYPE_STEEPLE_CHASE, _('Steeple Chase')),
        (TYPE_STEEPLE_CHASE_CROSS, _('Steeple Chase Cross Country')),
    )

    METEO_CHOICES = (
        (0x01, 'Soleil'),
        (0x02, 'Peu Nuageux'),
        (0x03, 'Très Nuageux'),
        (0x04, 'Couvert'),
        (0x05, 'Ondées Orageuses'),
        (0x06, 'Risque d\'Averses'),
        (0x07, 'Orages Isolés'),
        (0x0B, 'Brouillards'),
        (0x09, 'partiellement nuageux'),
        (0x0A, 'nuageux'),
        (0x0B, 'ciel dégagé'),
        (0x0C, 'brume') ,
        (0x0D, 'brouillard'),
        (0x0E, 'bruine légère'),
        (0x14, 'bruine'),
        (0x16, 'forte bruine'),
        (0x17, 'brume sèche'),
        (0x20, 'Pluies'),
        (0x21, 'Pluie faible'),
        (0x22, 'légère pluie'),
        (0x23, 'pluie modérée'),
        (0x24, 'pluie très fine'),
        (0x40, 'Neige'),
        (0x41, 'Pluie et Neige'),
        (0x42, 'Pluie et Averses de Neige'),
        (0x43, 'Variable, Averses Neige'),
        (0x44, 'légères chutes de neige'),
        (0x45, 'chutes de neige'),
    )
    TRACK_STATE_CHOICES = (
        (0x00, 'Bon terrain'), (0x01, 'Terrain collant'),
        (0x02, 'Terrain très souple'), (0x03, 'Terrain souple'),
        (0x04, 'Terrain lourd'), (0x05, 'Terrain très lourd'),
        (0x06, 'Terrain bon souple'), (0x07, 'Terrain assez souple'),
        (0x08, 'Terrain'), (0x09, 'Terrain léger'),
        (0x0A, 'Terrain standard (PSF)'), (0x0B, 'Terrain bon'),
        (0x0C, 'Terrain très léger'), (0x0D, 'Terrain bon léger'),
        (0x0E, 'Terrain ferme'), (0x0F, 'Terrain dur'),
        (0x10, 'Terrain  (PSF)'), (0x11, 'Bon'),
        (0x12, 'Collant'), (0x13, 'PSF Standard'),
        (0x14, 'Très souple'), (0x15, 'Souple'),
        (0x16, 'Très lourd'), (0x17, 'Lourd'),
        (0x18, 'Bon souple'), (0x19, 'Léger'),
        (0x1A, 'PSF Lente'), (0x1B, 'PSF Rapide'),
        (0x1C, 'Bon léger'),
    )
    ROPE_RIGHT = 0x01
    ROPE_LEFT = 0x02
    ROPE_CHOICES = (
        (ROPE_RIGHT, _('right')),
        (ROPE_LEFT, _('left'))
    )

    track = models.ForeignKey(Track, models.CASCADE, verbose_name=_('track'), db_index=True)
    pmu_meeting = models.SmallIntegerField(_('pmu meeting'), db_index=True)
    number = models.SmallIntegerField(_("pmu race number"), db_index=True)
    name = models.CharField(_('name'), max_length=64)
    date = models.DateTimeField(_('date'), db_index=True)

    type = models.SmallIntegerField(_('type'), choices=TYPE_CHOICES, db_index=True,
        null=True, blank=True)
    cancelled = models.BooleanField(_('cancelled'), default=False)
    jumping = models.BooleanField(_('jumping'), default=False)
    distance = models.PositiveIntegerField(_('distance'))
    allowance = models.PositiveIntegerField(_('allowance'), null=True, blank=True)
    stake = models.PositiveIntegerField(_('stake'), null=True, blank=True)
    rope = models.PositiveSmallIntegerField(_('rope'), choices=ROPE_CHOICES, null=True, blank=True)
    track_state = models.PositiveIntegerField(
        _('track state'), choices=TRACK_STATE_CHOICES, null=True, blank=True
    )

    # corde

    class Meta:
        verbose_name = _('race')
        verbose_name_plural = _('races')
        # constraints = (
        #    models.UniqueConstraint('track', 'type', 'source_id', name='unique_race_track'),
        #)


class Runner(models.Model):
    """ Horse's information for a specific race. """
    BLINDER_NONE = 0x00
    BLINDER_SOME = 0x01
    BLINDER_AUSTRALIAN = 0x02
    BLINDER_CHOICES = ((BLINDER_NONE, _('none')),
                       (BLINDER_SOME, _('blinder')),
                       (BLINDER_AUSTRALIAN, _('australian')))

    SHOE_NONE = 0b00000000
    SHOE_FRONT = 0b00000001
    SHOE_BACK =  0b00000010
    SHOE_BOTH = SHOE_FRONT | SHOE_BACK
    SHOE_MASK = SHOE_BOTH
    SHOE_PROTECTED_FRONT = 0b00000100
    SHOE_PROTECTED_BACK =  0b00001000
    SHOE_PROTECTED_BOTH =  SHOE_PROTECTED_FRONT | SHOE_PROTECTED_BACK
    SHOE_CHOICES = (
        (SHOE_NONE, 'DD'), (SHOE_FRONT, 'DF'), (SHOE_PROTECTED_FRONT, 'DP'),
        (SHOE_BACK, 'FD'), (SHOE_BOTH, 'FF'),
        (SHOE_BACK | SHOE_PROTECTED_FRONT, 'FP'),
        (SHOE_PROTECTED_BACK, 'PD'),
        (SHOE_PROTECTED_BACK | SHOE_FRONT, 'PF'),
        (SHOE_PROTECTED_BOTH, 'PP')
    )

    create_date = models.DateTimeField(_('creation date'), auto_now_add=True)
    update_date = models.DateTimeField(_('update date'), auto_now=True)

    race = models.ForeignKey(Race, models.CASCADE, verbose_name=_('race'), db_index=True)
    horse = models.ForeignKey(Horse, models.CASCADE, verbose_name=_('horse'), db_index=True)
    jockey = models.ForeignKey(Human, models.CASCADE, verbose_name=_('jockey'),
            related_name=_('jockey_runners'), db_index=True)
    # trainer = models.ForeignKey(Human, models.CASCADE, verbose_name=_('trainer'),
    #        related_name=_('trainer_runners'), db_index=True)
    team = models.CharField(_('stable'), max_length=1, null=True, blank=True)

    number = models.PositiveIntegerField()
    rail = models.SmallIntegerField(_('rail')) # corde
    non_runner = models.BooleanField(_('non runner'), default=False) # non partant
    distance = models.IntegerField(_('distance ran'), blank=True, null=True)        
    position = models.SmallIntegerField(_('arrival position'),
            null=True, blank=True)
    arrival_time = models.PositiveIntegerField(_('arrival time (msec)'),
            null=True, blank=True)
    arrival_length = models.DecimalField(_('arrival length (length)'),
            max_digits=6, decimal_places=2, null= True, blank=True)
    engagement = models.IntegerField(null=True, blank=True)
    supplement = models.IntegerField(null=True, blank=True)

    pregnant = models.BooleanField(_('pregnant'), default=False)
    shoe = models.SmallIntegerField(choices=SHOE_CHOICES, default=SHOE_NONE)
    blinder = models.SmallIntegerField(_('blinder'), choices=BLINDER_CHOICES,
            default=BLINDER_NONE)
    weight = models.PositiveIntegerField(_('weight bred'), null=True, blank=True)
    hat = models.BooleanField(_('hat'), default=False)
    tongue_tie = models.BooleanField(_('tongue tie'), default=False)

    # gains = models.PositiveIntegerField()
    odd_pmu = models.DecimalField(_('odd PMU'), max_digits=6, decimal_places=3,
            null=True, blank=True)
    odd_zeturf = models.DecimalField(_('odd Zeturf'), max_digits=6, decimal_places=3,
            null=True, blank=True)
    year_earnings = models.PositiveIntegerField(_('year earnings'), default=0)
    prev_year_earnings = models.PositiveIntegerField(_('previous year earnings'), default=0)
    total_victories_earnings = models.PositiveIntegerField(_('total victories earnings'), default=0)

    redkm = models.PositiveIntegerField(_('reduction kilometrique (msec)'), null=True, blank=True)
    record = models.IntegerField(default=0)
    first_race = models.BooleanField(default=False, blank=True, null=True)
    handicap = models.FloatField(default=0)
    handicap_distance = models.FloatField(default=0)
    handicap_weight = models.FloatField(default=0)
    enquiry = models.BooleanField(_('enquiry'), default=False)

    class Meta:
        verbose_name = _('runner')
        verbose_name_plural = _('runners')
        # constraints = (models.UniqueConstraint('race', 'horse', name='unique_runner'),)
        # Constaint: Horse sex & pregnancy

    @property
    def max_odd(self):
        return max(self.odd_pmu, self.odd_zeturf)

    @cached_property
    def arrival_length_float(self):
        return length_to_float(self.arrival_length)

# class RaceHuman(models.Model):
#     TYPE_TRAINER = Human.TYPE_TRAINER
#     TYPE_JOCKEY = Human.TYPE_JOCKEY
#     TYPE_CHOICES = ((TYPE_TRAINER, _('Trainer')),
#                     (TYPE_JOCKEY, _('Jockey')),)
#     
#     runner = models.ForeignKey(Runner, models.CASCADE)
#     human = models.ForeignKey(Human, models.CASCADE)
#     type = models.SmallIntegerField(choices=TYPE_CHOICES)
# 
#     wins_hippo_tx = models.DecimalField(max_digits=5, decimal_places=2, default=0.0)
#     places_hippo_tx = models.DecimalField(max_digits=5, decimal_places=2, default=0.0)
#     races_hippo = models.SmallIntegerField(default=0)
#     day_races = models.SmallIntegerField(default=0)
#     day_runs = models.SmallIntegerField(default=0)
#     day_wins = models.SmallIntegerField(default=0)
# 
#     class Meta:
#         verbose_name = _('race human')
#         verbose_name_plural = _('race humans')
#         # constraints = (
#         #    models.UniqueConstraint('runner', 'human', 'type', name='unique_runner_human'),
#         # )
# 


class Weather(models.Model):
    race = models.OneToOneField(Race, models.CASCADE, verbose_name=_('race'))
    update_date = models.DateTimeField(_('update date'))

    temperature = models.IntegerField(_('temperature'))
    clouds_bing = models.IntegerField(_('clouds'), null=True, blank=True)
    """ Bingoal """
    clouds_code = models.CharField(_('clouds code'), max_length=6, null=True, blank=True)
    """ Meteo France's code """
    wind = models.FloatField(_('wind (km/h)'), null=True, blank=True)
    wind_direction = models.CharField(_('wind direction'), max_length=6)
    wind_degree = models.IntegerField(_('wind direction (degree)'))
    rain = models.IntegerField(_('rain'), null=True, blank=True)
    lightning = models.BooleanField(_('lightning'), default=False)

    class Meta:
        verbose_name = _('meteo')
        verbose_name_plural = _('meteos')

