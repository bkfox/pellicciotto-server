from datetime import date, datetime
from django.db import models
from django.utils.translation import gettext_lazy as _


__all__ = ('Track', 'Stats', 'Human', 'Horse')


class Track(models.Model):
    name = models.CharField(_('name'), max_length=64, db_index=True)
    long_name = models.CharField(_('long name'), max_length=128, default='')
    country = models.CharField(_('country'), max_length=64, default='')
    country_code = models.CharField(_('country code'), max_length=6, default='')

    def save(self, *args, **kwargs):
        self.name = self.name and self.name.lower()
        self.long_name = self.long_name and self.long_name.lower()
        self.country = self.country and self.country.lower()
        super().save(*args, **kwargs)


class Stats(models.Model):
    rank = models.CharField(_('rank'), max_length=64, default='')
    computed_rank = models.CharField(_('rank'), max_length=64, default='')
    races = models.IntegerField(default=0)
    wins = models.IntegerField(default=0)
    places = models.IntegerField(default=0)
    places_second = models.IntegerField(default=0)
    places_third = models.IntegerField(default=0)
    create_date = models.DateTimeField(_('creation date'), auto_now_add=True)
    update_date = models.DateTimeField(_('update date'), auto_now=True)

    @property
    def wins_tx(self):
        return self.wins / self.races

    @property
    def places_tx(self):
        return self.places / self.races

    @property
    def places_second_tx(self):
        return self.places_second / self.races

    @property
    def places_third_tx(self):
        return self.places_third / self.races

    def save(self, *args, **kwargs):
        if self.places is None:
            self.places = (self.places_second or 0) + (self.places_third or 0)
        return super().save(*args, **kwargs)


class HumanQuerySet(models.QuerySet):
    def owner(self):
        return self.filter(type=Human.TYPE_OWNER)

    def jockey(self):
        return self.filter(type=Human.TYPE_JOCKEY)

    def trainer(self):
        return self.filter(type=Human.TYPE_TRAINER)


class Human(Stats):
    TYPE_TRAINER = 0x01
    TYPE_JOCKEY = 0x02
    TYPE_OWNER = 0x03
    TYPE_CHOICES = ((TYPE_TRAINER, _('Trainer')), (TYPE_JOCKEY, _('Jockey')),
                    (TYPE_OWNER, _('Owner')))
    
    name = models.CharField(_('name'), max_length=64, db_index=True)
    type = models.SmallIntegerField(choices=TYPE_CHOICES, null=True, blank=True)

    objects = HumanQuerySet.as_manager()

    class Meta:
        unique_together = ('name', 'type')


class Horse(Stats):
    SEX_MALE = 0x01
    SEX_FEMALE = 0x02
    SEX_GELDING = 0x03
    SEX_CHOICES = ((SEX_MALE, _('male')), (SEX_FEMALE, _('female')), (SEX_GELDING, _('gelding')))

    name = models.CharField(_('name'), max_length=64, unique=True, db_index=True)
    slug = models.SlugField(_('slug'), max_length=64, unique=True, db_index=True)
    owner = models.ForeignKey(Human, models.CASCADE, verbose_name=_('owner'),
                    related_name=_('owner_horses'), null=True)
    trainer = models.ForeignKey(Human, models.CASCADE, verbose_name=_('trainer'),
                    related_name=_('trainer_horses'))

    father = models.CharField(_('father'), max_length=25, null=True, blank=True) # : pere
    mother = models.CharField(_('mother'), max_length=25, null=True, blank=True) # : mere
    sex = models.SmallIntegerField(_('sex'), choices=SEX_CHOICES, null=True, blank=True) # : sexe
    race = models.CharField(_('horse race'), max_length=32, default='')
    age = models.PositiveIntegerField(_('age'), null=True, blank=True) # : age
    birth_date = models.DateTimeField(_('birth date'), null=True, blank=True)

    profits_career = models.FloatField(null=True, blank=True) # : gainscarriere
    profits_wins = models.FloatField(null=True, blank=True) # : gainsvictoires
    profits_place = models.FloatField(null=True, blank=True) # : gainsplace
    profits_current_year = models.FloatField(null=True, blank=True) # : gainsanneeencours
    profits_last_year = models.FloatField(null=True, blank=True) # : gainsanneeprecedente

    def save(self, *args, **kwargs):
        if self.age is None and self.birth_date:
            delta = datetime.now() - self.birth_date
            self.age = delta.years
        return super().save(*args, **kwargs)

