import threading
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from . import models, serializers, filters


__all__ = ('TrackViewSet', 'HumanViewSet', 'HorseViewSet', 'RaceViewSet',
            'RunnerViewSet',)

class TrackViewSet(viewsets.ModelViewSet):
    paginate_by = 100
    queryset = models.Track.objects.all().order_by('name')
    serializer_class = serializers.TrackSerializer
    filterset_class = filters.TrackFilters

class HumanViewSet(viewsets.ModelViewSet):
    paginate_by = 100
    queryset = models.Human.objects.all().order_by('name')
    serializer_class = serializers.HumanSerializer
    filterset_class = filters.HumanFilters

class HorseViewSet(viewsets.ModelViewSet):
    paginate_by = 100
    queryset = models.Horse.objects.all().order_by('name')
    serializer_class = serializers.HorseSerializer
    filterset_class = filters.HorseFilters

class RaceViewSet(viewsets.ModelViewSet):
    paginate_by = 100
    queryset = models.Race.objects.all().order_by('-date')
    serializer_class = serializers.RaceSerializer
    filterset_class = filters.RaceFilters
    
class RunnerViewSet(viewsets.ModelViewSet):
    paginate_by = 100
    queryset = models.Runner.objects.all().order_by('-number')
    serializer_class = serializers.RunnerSerializer
    filterset_class = filters.RunnerFilters

