from django.db import models
from rest_framework.parsers import JSONParser
from rest_framework.serializers import Serializer

from tools.tasks.http_request import JsonRequest

from ..models import Human, Horse, Race
from . import serializers
from .data import RaceReaders


__all__ = ('EquidiaApi', 'FetchHorse', 'RaceReaders', 'FetchRace')


# Entry points
# v2/courses/YYYY-MM-DD/R1/C1 -> infos de la course
# v2/courses/YYYY-MM-DD/R1/C1/rapports -> paris & quote
# v2/tracking/YYYY-MM-DD/R1/C1 -> résultats
# 


class EquidiaApi(JsonRequest):
    url = 'https://api.equidia.fr/api/public'
    headers = {
        'Referer': 'https://www.equidia.fr/',
    }

    def request(self, *args, **kwargs):
        response = super().request(*args, **kwargs)
        instance = kwargs.get('instance')
        if isinstance(self, MultipleDataMixin) and response.data:
            data, ser = {}, None
            for key, dat in response.data[0]:
                if isinstance(dat, Serializer) and dat.is_valid():
                    ser = ser or dat
                    data.update(dat.validated_data)
                elif isinstance(dat, dict):
                    data.update(dat)
            if instance:
                ser.instance = instance
            if ser:
                instance = ser.save(**datas)
        setattr(response, 'instance', instance)
        return response


#class FetchHorse(EquidiaApi):
#    """
#    Fetch horse data. ``key`` is used as horse name by default.
#
#    Detail Horse info seems not accessible from api, but integrated into HTML's
#    page showing horse info as JSON representation
#    """
#    url = EquidiaApi.url + '/chevaux/{name}/basic'
#    readers = {
#        'horse': data.Reader('$.data_cheval', serializers.HorseSerializer),
#        'rank': data.Reader('$.musique', serializers.HorseMusiqueSerializer),
#    }
#    name = None
#
#    def request(self, name, url=None, *args, **kwargs):
#        url = url or self.url
#        name = name or self.name
#        if not name:
#            raise ValueError('no name has been specified')
#        url = url.format(name=name)
#        return super().request(*args, url=url, name=name, **kwargs)
#

class FetchRace(EquidiaApi):
    url = EquidiaApi.url + '/courses/{date}/{meeting}/{race}'
    date = None
    """ Race date """
    meeting = None
    """ Meeting number """
    race = None
    """ Race number """
    reader = RaceReaders

    def __init__(self, key, date=None, meeting=None, race=None, **kwargs):
        """
        :param key: either key or tuple of ``(date, meeting, race)``.
        """
        if isinstance(key, tuple):
            try:
                date, meeting, race = key
                meeting, race = int(meeting), int(race)
            except:
                raise ValueError('When ``key`` is a tuple, it should be a tuple '
                                 'of ``(date, meeting, race)``.')
        super().__init__(key, date=date, meeting=meeting, race=race, **kwargs) 

    def request(self, date=None, meeting=None, race=None, **kwargs):
        date = date or self.date
        meeting = meeting or self.meeting
        race = race or self.race
        url = url or self.url
        if not (date and meeting and race):
            raise ValueError('one of `date`, `meeting`, `race` is missing')
        return super().request(url=url, date=date, meeting=meeting, race=race, **kwargs)

