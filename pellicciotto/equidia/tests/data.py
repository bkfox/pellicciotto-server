from tools.data import *
from tools.test import for_each_sample

from pellicciotto import models
from .. import data, serializers
from .base import RaceTestCase


__all__ = ('RaceReadersTestCase',)


class RaceReadersTestCase(RaceTestCase):
    def setUp(self):
        self.readers = data.RaceReaders()
        self.pool = Pool()

    @for_each_sample
    def test_read(self, path, data):
        ser = serializers.RaceSerializer(data=data)
        ser.is_valid(raise_exception=True)
        race_data = Record(ser.validated_data)
        race_index = self.readers.race_index(race_data)

        pool = Pool()
        self.readers.read(data, pool=pool, save=True)

        # test runners
        for runner in pool.runners:
            pass
            

