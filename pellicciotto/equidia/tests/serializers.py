from datetime import datetime, time

from tools.test import SerializerTestCase
from tools.data import *

from pellicciotto import models
from .. import serializers
from .base import RaceTestCase


__all__ = ('TrainerSerializerTestCase', 'OwnerSerializerTestCase', 'JockeySerializerTestCase',
            'BaseHorseSerializerTestCase', 'TrackSerializerTestCase', 'RaceSerializerTestCase')

# __all__ = ('RaceSerializerTestCase', 'RunnerSerializerTestCase')


class TrainerSerializerTestCase(SerializerTestCase, RaceTestCase):
    reader = RaceTestCase.readers['trainers']
    serializer_class = serializers.TrainerSerializer

    def test_deserialize(self):
        def test(key, source, result):
            self.assertEquals(result['name'], source['nom_entraineur'])
            self.assertEquals(result['type'], models.Human.TYPE_TRAINER)
            
        results = self.deserialize_many(self.files.items(), as_data=True, many=True)
        self.zip_test(self.files, results, test, many=True)


class OwnerSerializerTestCase(SerializerTestCase, RaceTestCase):
    reader = RaceTestCase.readers['owners']
    serializer_class = serializers.OwnerSerializer

    def test_deserialize(self):
        def test(key, source, result):
            self.assertEquals(result['name'], source['nom_proprietaire'])
            self.assertEquals(result['type'], models.Human.TYPE_OWNER)
            
        results = self.deserialize_many(self.files.items(), many=True)
        self.zip_test(self.files, results, test, many=True)


class JockeySerializerTestCase(SerializerTestCase, RaceTestCase):
    reader = RaceTestCase.readers['jockeys']
    serializer_class = serializers.JockeySerializer

    def test_deserialize(self):
        def test(key, source, result):
            self.assertEquals(result['name'], source['nom_monte'])
            self.assertEquals(result['type'], models.Human.TYPE_JOCKEY)

        results = self.deserialize_many(self.files.items(), many=True)
        self.zip_test(self.files, results, test, many=True)


class BaseHorseSerializerTestCase(SerializerTestCase, RaceTestCase):
    reader = Reader('$.partants.*.cheval', many=True)
    serializer_class = serializers.BaseHorseSerializer

    def test_deserialize(self):
        def test(key, source, result):
            self.assertEquals(result['name'], source['nom_cheval'])
            self.assertEquals(result['slug'], source['slug'])
            self.assertEquals(result['age'], source['age_cheval'])
            self.assertEquals(result['rank'], source['musique'])
            self.assertEquals(result['profits_career'], source['gains_carriere'])
            # self.assertEquals(result[''], source[''])

        results = self.deserialize_many(self.files.items(), many=True, pool=pool)
        self.zip_test(self.files, results, test, many=True)


class TrackSerializerTestCase(SerializerTestCase, RaceTestCase):
    reader = Reader('$.reunion.hippodrom')
    serializer_class = serializers.TrackSerializer

    def test_deserialize(self):
        def test(key, source, result):
            self.assertEquals(result['long_name'].lower(), source['name'].lower())

        results = self.deserialize_many(self.files.items())
        self.zip_test(self.files, results, test)


class RaceSerializerTestCase(SerializerTestCase, RaceTestCase):
    serializer_class = serializers.RaceSerializer

    def test_deserialize(self):
        def test(key, source, result):
            meeting = source['reunion']
            self.assertNotIn('reunion', result)
            self.assertIsInstance(result['date'], datetime)
            self.assertEquals(result['pmu_meeting'], meeting['num_reunion'])
            self.assertEquals(result['date'].date(), meeting['date_reunion'])
            self.assertEquals(result['date'].time(), source['heure_depart_course'])
            self.assertEquals(result['stake'], source['enjeu_s_g']['montant'])
            self.assertEquals(result['track']['country'], source['pays']['label'])
            self.assertEquals(result['track']['country_code'], source['pays']['code'])
            self.assertEquals(source['num_partant'], result['number'])
            
        results = self.deserialize_many(self.files.items())
        self.zip_test(self.files, results, test)


# class RaceSerializerTestCase(TestCase):
#     data_path = __file__ + '/../../samples/equidia_race_*'
#     data_parser = json.loads
# 
#     def test_deserialize(self):
#         for path, data in self.files.items():
#             ser = RaceSerializer(data=data)
#             ser.is_valid(raise_exception=True)
#             vdat = ser.validated_data
# 
#     def test_save(self):
#         for path, data in self.files.items():
#             ser = RaceSerializer(data=data)
#             ser.is_valid(raise_exception=True)
#             obj = ser.save()
# 
# 
# class RunnerSerializerTestCase(TestCase):
#     data_path = RaceSerializerTestCase.data_path
#     data_parser = json.loads
# 
#     def test_deserialize(self):
#         for path, data in self.files.items():
#             data = data['partants']
#             ser = RunnerSerializer(data=data, many=True)
#             ser.is_valid(raise_exception=True)
#             vdat = ser.validated_data
#             self.assertEquals(len(vdat), len(data))
# 
#     def test_save(self):
#         for path, data in self.files.items():
#             ser = RaceSerializer(data=data)
#             ser.is_valid(raise_exception=True)
#             race = ser.save()
# 
#             runners = data['partants']
#             ser = RunnerSerializer(race, data=runners, many=True)
#             ser.is_valid(raise_exception=True)
#             obj = ser.save()
# 

