import json

from tools.data import *
from tools.test import SamplesTestCase
from pellicciotto import models

__all__ = ('RaceTestCase',)


class RaceTestCase(SamplesTestCase):
    """ Test case loading race data. """
    data_path = __file__ + '/../samples/equidia_race_*'
    data_parser = json.loads
    readers = Readers({
        'trainers': Reader('$.partants.*.cheval.entraineur', many=True),
        'owners': Reader('$.partants.*.cheval.proprietaire', many=True),
        'jockeys': Reader('$.partants.*.monte', many=True),
    })
     
    def get_pool(self):
        """ Get pool and fill it with files `by readers`. """
        self.pool = Pool({
            'trainers': RecordSet('name', models.Human),
            'owners': RecordSet('name', models.Human),
            'jockeys': RecordSet('name', models.Human),
            'horses': RecordSet('slug', models.Horse),
            'tracks': RecordSet('name', models.Track),
        })
        for data in self.files.values():
            self.readers.read(data, pool=self.pool)


