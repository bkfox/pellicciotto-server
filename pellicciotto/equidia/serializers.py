""" PMU.fr API's serializers. """
from datetime import datetime, time
from rest_framework import serializers

from tools.serializers import *
from tools.data import Relation
from .. import units
from ..models import Horse, Human, Track, Race, Runner


__all__ = ('TrainerSerializer', 'OwnerSerializer', 'HorseSerializer',
           'FullHorseSerializer',  'HorseMusiqueSerializer',
           'TrackSerializer', 'MeetingSerializer', 'CountrySerializer',
           'RaceSerializer')


class TrainerSerializer(serializers.ModelSerializer):
    # TODO: default type
    nom_entraineur = serializers.CharField(source='name')

    def run_validation(self, data):
        validated_data = super().run_validation(data)
        validated_data['type'] = Human.TYPE_TRAINER
        return validated_data

    class Meta:
        model = Human
        fields = ('nom_entraineur',)


class OwnerSerializer(serializers.ModelSerializer):
    # TODO: default type
    nom_proprietaire = serializers.CharField(source='name')

    def run_validation(self, data):
        validated_data = super().run_validation(data)
        validated_data['type'] = Human.TYPE_OWNER
        return validated_data

    class Meta:
        model = Human
        fields = ('nom_proprietaire',)


class JockeySerializer(serializers.ModelSerializer):
    # TODO: default type
    nom_monte = serializers.CharField(source='name')

    def run_validation(self, data):
        validated_data = super().run_validation(data)
        validated_data['type'] = Human.TYPE_JOCKEY
        return validated_data
        
    class Meta:
        model = Human
        fields = ('nom_monte',)


class BaseHorseSerializer(serializers.ModelSerializer):
    nom_cheval = serializers.CharField(source='name')
    slug = serializers.CharField()
    sexe = MapField(source='sex', default=None, map={
        'F': Horse.SEX_FEMALE, 'M': Horse.SEX_MALE,
        'H': Horse.SEX_GELDING
    })
    age_cheval = serializers.IntegerField(source='age', min_value=1, required=False)
    musique = serializers.CharField(source='rank', required=False)
    gains_carriere = FloatField(source='profits_career', required=False, default=None)
    
    entraineur = TrainerSerializer(source='trainer', required=False)
    proprietaire = OwnerSerializer(source='owner', required=False)

    class Meta:
        model = Horse
        fields = ('nom_cheval', 'slug', 'sexe', 'age_cheval',
                  'musique', 'gains_carriere', 'proprietaire', 'entraineur')
        list_serializer_class = ModelListSerializer
        pk_field = 'slug'


# ---- Horse
class HorseSerializer(BaseHorseSerializer):
    nom_pere = serializers.CharField(source='father')
    nom_mere = serializers.CharField(source='mother')
    race = serializers.CharField(required=False)
    date_naissance = serializers.DateField(source='birth_date')
    
    nb_courses_total = serializers.IntegerField(source='races')
    nb_premier_total = serializers.IntegerField(source='wins')
    nb_deuxieme_total = serializers.IntegerField(source='places_second')
    nb_troisieme_total = serializers.IntegerField(source='places_third')
    gains_victoire = FloatField(source='profits_wins')
    gains_place = FloatField(source='profits_place')

    # stable, profits_current_year, profits_last_year

    class Meta:
        model = Horse
        fields = BaseHorseSerializer.Meta.fields + ('nom_pere', 'nom_mere', 'race',
              'date_naissance', 'nb_courses_total', 'nb_premier_total',
              'nb_deuxieme_total', 'nb_troisieme_total', 'gains_victoire',
              'gains_place')
        list_serializer_class = ModelListSerializer
        pk_field = 'slug'


class HorseMusiqueSerializer(serializers.Serializer):
    cheval_calculated = serializers.CharField(source='computed_rank',
            required=False, default='')
    trainer_calculated = serializers.CharField(source='trainer.computed_rank',
            required=False, default='')

    class Meta:
        model = Horse
        fields = ('cheval_calculated', 'trainer_calculated')


# ---- Race
class TrackSerializer(serializers.Serializer):
    name = serializers.CharField()

    def run_validation(self, data):
        validated_data = super().run_validation(data)
        name = validated_data['name'].upper()
        validated_data['long_name'] = name
        validated_data['name'] = name.split('(')[0]
        return validated_data

    class Meta:
        model = Track
        fields = ('name',)


class MeetingSerializer(serializers.ModelSerializer):
    hippodrome = TrackSerializer(source='track')
    num_reunion = serializers.IntegerField(source='pmu_meeting')
    date_reunion = serializers.DateField()

    class Meta:
        fields = ('hippodrome', 'num_reunion', 'date_reunion')
        model = Race


class CountrySerializer(serializers.ModelSerializer):
    label = serializers.CharField(source='country', required=False)
    code = serializers.CharField(source='country_code', required=False)

    class Meta:
        model = Track
        fields = ('label', 'code')


class RaceSerializer(serializers.ModelSerializer):
    reunion = MeetingSerializer()
    pays = CountrySerializer()
    num_course_pmu = serializers.IntegerField(source='number')
    heure_depart_course = serializers.TimeField()
    liblong_prix_course = serializers.CharField(source='name')
    distance = serializers.IntegerField()
    # montant_total_allocation = CurrencyField(source='allocation', currency='euros', sep=',')
    discipline = MapField(source='type', map={
        'Attelé': Race.TYPE_HARNESS, 'Haies': Race.TYPE_FENCE, 'Monté': Race.TYPE_BRED,
        'Plat': Race.TYPE_FLAT, 'Steeple Chase': Race.TYPE_STEEPLE_CHASE,
        'Cross': Race.TYPE_STEEPLE_CHASE_CROSS
    }, default=None)
    enjeu_s_g = serializers.DictField(source='stake', required=False)
    montant_total_allocation = IntReprField(source='allowance', required=False)

    class Meta:
        model = Race
        fields = ('reunion', 'pays', 'num_course_pmu', 'heure_depart_course',
                  'liblong_prix_course', 'distance', 'discipline', 'enjeu_s_g',
                  'montant_total_allocation')

    def run_validation(self, data):
        """
        ``reunion`` validated data into self's ones.
        Get fields values for: date
        """
        vdat = super().run_validation(data)

        # race
        meeting = vdat.pop('reunion')
        vdat.update(meeting)
        vdat['date'] = datetime.combine(vdat.pop('date_reunion'),
                vdat.pop('heure_depart_course', None) or time(0,0))
        stake = vdat.pop('stake', None)
        if stake:
            vdat['stake'] = stake.get('montant')

        # track
        pays = vdat.pop('pays', None)
        if pays:
            vdat['track']['country'] = pays.get('label') or ''
            vdat['track']['country_code'] = pays.get('code') or ''
        return vdat


class RunnerHorseSerializer(BaseHorseSerializer):
    valeur = serializers.FloatField(required=False, default=0.0)

    class Meta:
        model = Horse
        fields = BaseHorseSerializer.Meta.fields + ('valeur',)


class RunnerSerializer(serializers.ModelSerializer):
    # FIXME: should be passed down/set by reader at a different place
    race = None
    """ Race related to this runner. MUST BE provided. """
    
    cheval = RunnerHorseSerializer(source='horse')
    monte = JockeySerializer(source='jockey')

    num_partant = serializers.IntegerField(source='number')
    place_corde_partant = serializers.IntegerField(source='rail', required=False)
    statut_part = MapField(source='non_runner', map={
        False: 'DP', True: 'NP'
    })
    num_place_arrivee = IntegerField(source='position', default=None)
    # either arrival_time or arrival_length
    temps_part = serializers.CharField(required=False) 
    distance = serializers.IntegerField(required=False)
    # engagement:
    # supplement:

    # pregnant:
    deferrer_partant = MapField(source='shoe', map={
        Runner.SHOE_BOTH: '', Runner.SHOE_BACK: '1', Runner.SHOE_FRONT: '2',
        Runner.SHOE_NONE: '3',  Runner.SHOE_PROTECTED_BOTH: '4',
        Runner.SHOE_BACK | Runner.SHOE_PROTECTED_FRONT: '5',
        Runner.SHOE_PROTECTED_FRONT: '6',
        Runner.SHOE_PROTECTED_BACK | Runner.SHOE_FRONT: '7',
        Runner.SHOE_PROTECTED_BACK: '8',
    }, default=Runner.SHOE_BOTH)
    oeil_partant = MapField(source='blinder', map={
        Runner.BLINDER_SOME: 'O', Runner.BLINDER_AUSTRALIAN: 'A',
        Runner.BLINDER_NONE: 'N'
    }, default=Runner.BLINDER_NONE)
    pds_cond_monte_partant = FloatField(source='weight', required=False, default=None)
    bonnet = serializers.BooleanField(source='hat', default=False)
    attache_langue = serializers.BooleanField(source='tongue_tie', default=False)

    reduction_km = TimerField(source='redkm', required=False)
    pds_calc_hand_partant = FloatField(source='handicap_weight', required=False, default=0.0)


    def __init__(self, race, *args, **kwargs):
        self.race = race
        super().__init__(*args, **kwargs)

    def run_validation(self, data):
        """
        Set values:
        - ``handicap``: from ``cheval.valeur``;
        - ``arrival_time`` / ``arrival_length``: from ``temp_part``;
        """
        vdat = super().run_validation(data)
        vdat['race'] = self.race

        # runner
        horse = vdat.get('horse')
        vdat['handicap'] = horse.pop('valeur', None)
 
        # arrival_time/length
        result = vdat.pop('temps_part', None)
        if result:
            if '"' in result:
                vdat['arrival_time'] = units.chrono_to_msecs(result)
            elif 'L' in result:
                vdat['arrival_length'] = units.length_to_float(result)
        return vdat

    def prepare_data(self, data):
        return data

    def create(self, data):
        data = self.prepare_data(data)
        return super().create(data)

    def update(self, instance, data):
        data = self.prepare_data(data)
        return super().update(instance, data)

    class Meta:
        model = Runner
        fields = ('cheval', 'monte', 'num_partant',
            'place_corde_partant', 'statut_part', 'num_place_arrivee',
            'temps_part', 'distance', 'deferrer_partant', 'oeil_partant',
            'pds_cond_monte_partant', 'bonnet', 'attache_langue',
            'reduction_km', 'pds_calc_hand_partant')


