from django.apps import AppConfig


class EquidiaConfig(AppConfig):
    name = 'pellicciotto.equidia'
    label = 'pellicciotto_equidia'
