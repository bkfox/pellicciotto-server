from django.db.models import Q

from tools.data import Reader, Readers, RecordSet, ModelRecordSet, Relation
from ..models import Horse, Human, Race, Runner, Track
from . import serializers


__all__ = ('RaceReaders',)


class RaceReaders(Readers):
    race = None
    """ Current race being read """
    
    @staticmethod
    def race_index(race):
        return (race.date, race.pmu_meeting, race.number)

    @staticmethod
    def race_lookup(queryset, indexes, items):
        filters = None
        for date, pmu_meeting, number in indexes:
            expr = Q(date=date, pmu_meeting=pmu_meeting, number=number)
            if filters is not None:
                filters = filters | expr
            else:
                filters = expr
        return queryset.filter(filters)

    readers = Readers([
        # FIXME: delete into get_data as kwargs
        ('tracks', Reader('$.reunion.hippodrome', serializers.TrackSerializer)),
        ('jockeys', Reader('$.partants.*.monte', serializers.JockeySerializer, many=True)),
        # ('owners', Reader('$.partants.*.cheval.proprietaire', serializers.OwnerSerializer, many=True)),
        ('trainers', Reader('$.partants.*.cheval.entraineur', serializers.TrainerSerializer, many=True)),
        ('horses', Reader('$.partants.*.cheval', serializers.RunnerHorseSerializer, many=True)),
        ('races', Reader('$', serializers.RaceSerializer)),
        ('runners', Reader('$.partants.*', serializers.RunnerSerializer, many=True)),
    ])

    record_sets = {
        'tracks': ModelRecordSet('name', Track),
        'jockeys': ModelRecordSet('name', queryset=Human.objects.jockey()),
        # 'owners': ModelRecordSet('name', queryset=Human.objects.owner()),
        'trainers': ModelRecordSet('name', queryset=Human.objects.trainer()),
        'horses': ModelRecordSet('slug', Horse, relations=(
             ('trainer', Relation('trainers', 'name', '$.trainer')),
             # ('owner', Relation('owners', 'name', '$.owner')), 
        )),
        'races': ModelRecordSet(race_index, Race, lookup=race_lookup, relations=(
            ('track', Relation('tracks', 'name', '$.track')),
        )),
        'runners': ModelRecordSet('name', Runner, lookup='horse__name', relations=(
             ('horse', Relation('horses', 'slug', '$.horse')),
             ('jockey', Relation('jockeys', 'name', '$.jockey')), 
        )),
    }

    race = None

    def read_one(self, key, *args, **kwargs):
        if key == 'runners':
            kwargs['race'] = self.race
        result = super().read_one(key, *args, **kwargs)
        if key == 'races' and result:
            self.race = result
        return result

    def get_pool_record_set(self, pool, key):
        record_set = super().get_pool_record_set(pool, key)
        if key == 'runners' and record_set:
            record_set.queryset = record_set.queryset.filter(race_id=self.race.id)
        return record_set


