from django.contrib import admin
from . import models

@admin.register(models.Track)
class TrackAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    
@admin.register(models.Human)
class HumanAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'type')
    search_fields = ('name',)

@admin.register(models.Horse)
class HorseAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ('name',)

@admin.register(models.Race)
class RaceAdmin(admin.ModelAdmin):
    list_display = ('id', 'type', 'track_id', 'date')

@admin.register(models.Runner)
class RunnerAdmin(admin.ModelAdmin):
    list_display = ('id', 'race_id', 'horse_id', 'number')

