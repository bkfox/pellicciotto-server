import django_filters as filters

from . import models


__all__ = ('TrackFilters', 'HumanFilters', 'HorseFilters', 'RaceFilters',
            'RunnerFilters',)


class TrackFilters(filters.FilterSet):
    class Meta:
        model = models.Track
        fields = {
            'name': ['icontains', 'iexact'],
        }

class HumanFilters(filters.FilterSet):
    class Meta:
        model = models.Human
        fields = {
            'type': ['exact', 'in'],
            'name': ['icontains', 'iexact']
        }

class HorseFilters(filters.FilterSet):
    class Meta:
        model = models.Horse
        fields = {
            'name': ['icontains', 'iexact'],
        }

class RaceFilters(filters.FilterSet):
    class Meta:
        model = models.Race
        fields = {
            'type': ['exact', 'in'],
            'track': ['exact', 'in'],
            'date': ['exact', 'gte', 'lte'],
        }

class RunnerFilters(filters.FilterSet):
    class Meta:
        model = models.Runner
        fields = {
            'horse': ['exact', 'in'],
            'race': ['exact'],
            'jockey': ['exact', 'in'],
        }

