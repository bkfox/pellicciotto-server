from django.apps import AppConfig


class PellicciottoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pellicciotto'
