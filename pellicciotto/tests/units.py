from django.test import TestCase
from pellicciotto import units

class UnitsTestCase(TestCase):
    def test_length_to_float(self):
        items = {'nez': 0.05, 'cte. tete': 0.08,
                 '1L': 1.0, '1/ 2 L ': 0.5,
                 '1 L1/2': 1.5, '2 L 3/4': 2.75}

        for value, expected in items.items():
            result = units.length_to_float(value)
            self.assertEquals(result, expected,
                "{}!={} for length {}".format(result, expected, value))

    def test_chrono_to_msecs(self):
        items = {'1\'12"13': 72013, '0\'01"40': 1040}

        for value, expected in items.items():
            result = units.chrono_to_msecs(value)
            self.assertEquals(result, expected,
                "{}!={} for length {}".format(result, expected, value))
