
__all__ = ('lengths_names', 'length_to_float', 'chrono_to_msecs')


lengths_names = {
    # equivalences from PMU
    'nez': 0.05,
    'cte tete': 0.08, 'courte tete': 0.08, 
    'tete': 0.1,
    'cte enc': 0.15, 'courte encolure': 0.15, 
    'encolure': 0.25,
}
""" Length name to length """


def length_to_float(value):
    """ Return float for provided length """
    if isinstance(value, float):
        return value
    if not isinstance(value, str):
        raise ValueError('value must be a string or a float')
    value = value.lower().replace('.', '').strip()
    if value in lengths_names:
        return lengths_names[value]

    length, decimal = value.split('l')
    decimal = decimal.strip()
    if '/' in length:
        num, den = length.split('/')
        length = 0
    elif decimal and '/' in decimal:
        num, den = decimal.split('/')
    else:
        num, den = 0, 1
    return int(length) + float(num) / float(den)


def chrono_to_msecs(value):
    """ Chrono timer into milliseconds (format: `MM'SS"MS`). """
    if not isinstance(value, str) or "'" not in value:
        return
    m, s = value.split("'")
    s, ms = s.split('"')
    return int(m)*60*1000 + int(s)*1000 + int(ms)



