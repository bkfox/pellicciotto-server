from django.apps import AppConfig


class AspiturfConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'aspiturf'
