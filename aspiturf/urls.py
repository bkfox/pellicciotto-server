from django.urls import path, include
from rest_framework.routers import DefaultRouter

from . import views, viewsets

__all__ = ('urls', 'router')

router = DefaultRouter()
router.register('cachedates', viewsets.CacheDateViewSet, 'cache_date')
# router.register('caractrap', viewsets.CaracTrapViewSet, 'caractrap')

urls = [
    path('', views.AppView.as_view(), name='aspiturf-app'),
    path('api/', include(router.urls)),
]

