import django_filters as filters

from .models import CacheDate


class CacheDateFilters(filters.FilterSet):
    class Meta:
        model = CacheDate
        fields = {
            'comp': ['exact'],
            'jour': ['exact', 'gt', 'lt'],
            'hippo': ['iexact'],
            'typec': ['iexact'],
            'idche': ['exact'],
        }


