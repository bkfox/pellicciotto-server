from django.core.paginator import Paginator
from django.views.generic import ListView

from pellicciotto.views import View
from tools.mixins import FiltersMixin

from .models import CacheDate, CaracTrap
from .filters import CacheDateFilters


__all__ = ('AppView', 'CacheDateListView',)


class AppView(View):
    template_name = 'aspiturf/main.html'

    def get_context_data(self, **kwargs):
        kwargs['CacheDate'] = CacheDate
        return super().get_context_data(**kwargs)


