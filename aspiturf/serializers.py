from rest_framework.serializers import ModelSerializer
from .models import CacheDate, CaracTrap


__all__ = ('CacheDateSerializer', 'CaracTrapSerializer')


class CacheDateSerializer(ModelSerializer):
    class Meta:
        model = CacheDate
        fields = '__all__'


class CaracTrapSerializer(ModelSerializer):
    class Meta:
        model = CaracTrap
        fields = '__all__'

