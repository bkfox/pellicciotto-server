import threading
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from tools.viewsets import PoolViewSet
from tools.pool import Pool
from .filters import CacheDateFilters
from .models import CacheDate, CaracTrap
from .serializers import CacheDateSerializer, CaracTrapSerializer
from .tasks import AspiTurfImports


def runImport(pool, dataset, count, **kwargs):
    if pool and pool.is_running:
        return

    tasks = AspiTurfImporter.split('aspiturf-import', dataset,
                                    chunk_size=512, count=count)
    pool.submit(tasks)
    pool.run(**kwargs)


class CacheDateViewSet(PoolViewSet, viewsets.ModelViewSet):
    pool = Pool(max_workers=4)
    paginate_by = 100
    queryset = CacheDate.objects.all().order_by('-id')
    serializer_class = CacheDateSerializer
    filterset_class = CacheDateFilters

    @action(detail=False, methods=['POST'])
    def run_import(self, request):
        if not self.pool.is_running:
            queryset = self.get_queryset().not_imported()
            queryset = self.filter_queryset(queryset)
            count = 100000
            t = threading.Thread(target=runImport,args=[self.pool,queryset,count])
            t.setDaemon(True)
            t.start()
        return self.status(request)

