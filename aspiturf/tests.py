from django.test import TransactionTestCase

from pellicciotto import models
from tools import Pool

from .models import CacheDate
from .tasks import AspiTurfImports


class ImportTestCase(TransactionTestCase):
    fixtures = ['aspiturf/test_fixture.json']
    pool = None
    importer = None


    import_rels = (
        (models.Horse.objects.all(), 'idche', 'source_id'),
        (models.Track.objects.all(), 'hippo', 'name'),
        (models.Human.objects.filter(type=models.Human.TYPE_JOCKEY), 'idjockey', 'source_id'),
        (models.Human.objects.filter(type=models.Human.TYPE_TRAINER), 'identraineur', 'source_id'),
        (models.Race.objects.all(), 'numcourse', 'source_id'),
        (models.Runner.objects.all(), 'id', 'source_id'),
    )
    

    def setUp(self):
        dataset = CacheDate.objects.all()
        self.pool = Pool(max_workers=4)
        self.importers = AspiTurfImports.split('aspiturf', dataset)
        self.pool.submit(self.importers)

    def test_import_1(self):
        self.pool.run()

        for query, source_attr, target_attr in self.import_rels:
            targets = set(query.values_list(target_attr, flat=True).distinct())
            # self.assertNotEquals(len(targets), 0,
            #    """ Empty targets ({} -> {}.{}) """.format(
            #    source_attr, query.model.__name__, target_attr
            #))

            sources = set(CacheDate.objects.filter(**{source_attr + '__in': targets})
                                .exclude(**{source_attr: 0})
                                .values_list(source_attr, flat=True).distinct())

            if len(targets) != len(sources):
                self.assertEquals(len(targets), len(sources),
                    """ Missing CacheDates: {} """.format(
                    ['{}'.format(v) for v in targets if v not in sources][:10]
                ))
            

    def test_import_2(self):
        self.pool.run()

        for query, source_attr, target_attr in self.import_rels:
            targets = set(query.values_list(target_attr, flat=True).distinct())
            sources = set(CacheDate.objects.filter(**{source_attr + '__isnull': False})
                                .exclude(**{source_attr: 0})
                                .values_list(source_attr, flat=True).distinct())

            if len(targets) != len(sources):
                self.assertEquals(len(targets), len(sources),
                    """ Missing CacheDates ({} -> {}.{}):  {} """.format(
                    source_attr, query.model.__name__, target_attr,
                    ', '.join(['{}'.format(v) for v in sources if v not in targets][:10])
                ))
            

    

