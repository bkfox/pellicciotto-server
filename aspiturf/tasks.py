from datetime import date, datetime, timedelta
from zoneinfo import ZoneInfo

from pellicciotto.models import *
from tools.tasks import task, wait
from tools.tasks.import_model import import_model, ImportModels
from .models import *


__all__ = ('AspiTurfImports',)


RACE_TYPES_MAP = {
    'Attelé': Race.TYPE_HARNESS,
    'Haies': Race.TYPE_FENCE,
    'Monté': Race.TYPE_BRED,
    'Plat': Race.TYPE_FLAT,
    'Steeple-chase': Race.TYPE_STEEPLE_CHASE,
    'Steeple-chase cross-country': Race.TYPE_STEEPLE_CHASE_CROSS,
}
SEX_MAP = {
    'F': Horse.SEX_FEMALE,
    'H': Horse.SEX_GELDING,
    'M': Horse.SEX_MALE,
    'X': None,
}
METEO_MAP = {l: k for k,l in Race.METEO_CHOICES}
TRACK_STATE_MAP = {l: k for k,l in Race.TRACK_STATE_CHOICES}
SHOE_MAP = {l: k for k,l in Runner.SHOE_CHOICES}
BLINDER_MAP = {'O': Runner.BLINDER_SOME, 'A': Runner.BLINDER_AUSTRALIAN}

# Map stats values to: (horse, trainer, jockey, couple)
STATS_MAPS = {
        'name': ('cheval', 'entraineur', 'jockey', None),
        'rank': ('musiqueche', 'musiqueent', 'musiquejoc', None), 
        'races': ('coursescheval', 'coursesentraineur', 'coursesjockey', 'nbcoursecouple'), 
        'wins': ('victoirescheval', 'victoiresentraineur', 'victoiresjockey', 'nbvictcouple'), 
        'places': ('placescheval', 'placeentraineur', 'placejockey', 'nbplacecouple'), 
        'wins_tx': ('pourcvictcheval', 'pourcvictent', 'pourcvictjock', 'txvictcouple'), 
        'places_tx': ('pourcplacecheval', 'pourcplaceent', 'pourcplacejock', 'txplacecouple'), 
        'wins_hippo_tx': ('pourcvictchevalhippo', 'pourcvictenthippo', 'pourcvictjockhippo', 'txplacecouplehippo'),
        'places_hippo_tx': ('pourcplacechevalhippo', 'pourcplaceenthippo', 'pourcplacejockhippo', 'txplacecouplehippo'),
        'races_hippo_count': ('nbrcoursechevalhippo', 'nbrcourseenthippo', 'nbrcoursejockhippo', 'nbcoursecouplehippo'),
        'day_races': (None, 'montesduentraineurjour', 'montesdujockeyjour', None), 
        'day_runs': (None, 'courueentraineurjour', 'couruejockeyjour', None), 
        'day_wins': (None, 'victoireentraineurjour', 'victoirejockeyjour', None),
}
STATS_HORSE, STATS_TRAINER, STATS_JOCKEY, STATS_COUPLE = 0, 1, 2, 3


def to_int(n):
    if n is None:
        return 0
    return int(''.join(i for i in n if i.isdigit()) or 0)

def to_decimal(n, sep='.'):
    if n is None:
        return 0
    return int(''.join(i for i in n if i.isdigit() or i == sep) or 0)

def to_msecs(s):
    if not s or "'" not in s:
        return 0
    a, b = s.split("'")
    b, c = b.split('"')
    return (int(a or 0) * 60 + int(b or 0)) * 1000 + int(c or 0) * 10


class AspiTurfImports(ImportModels):
    timezone = 'Europe/Paris'

    def update_stats(self, source, target, offset):
        for target_attr, source_attrs in STATS_MAPS.items():
             if not hasattr(target, target_attr):
                 continue
             source_attr = source_attrs[offset]
             if source_attr is None:
                 continue

             source_value = getattr(source, source_attr, None)
             setattr(target, target_attr, source_value)
        return target

    @task(priority=-1)
    def caractraps(self, dataset=None, results=None, **kwargs):
        ids = set(c.comp for c in dataset)
        caractraps = CaracTrap.objects.filter(comp__in = ids)
        results['caractraps'] = { c.comp: c for c in caractraps }

    # Track
    @import_model(Track, 'hippo', 'name', priority=0)
    def tracks(self, source, results, target=None, **kwargs):
        return target or Track(name=source.hippo)

    # Horse
    @import_model(Horse, 'idche', 'source_id', priority=1)
    def horses(self, source, results, target, **kwargs):
        if not target:
            # FIXME: year may be wrong
            target = Horse(
                father = source.pere,
                mother = source.mere,
                sex = SEX_MAP.get(source.sexe),
                age = source.age,
            )
        return self.update_stats(source, target, STATS_HORSE)

    # Humans
    @import_model(Human, 'idjockey', 'source_id', priority=1)
    def jockeys(self, source, results, target=None, **kwargs):
        target = target or Human(type=Human.TYPE_JOCKEY)
        return self.update_stats(source, target, STATS_JOCKEY)

    @import_model(Human, 'identraineur', 'source_id', priority=1)
    def trainers(self, source, results, target=None, **kwargs):
        target = target or Human(type=Human.TYPE_TRAINER)
        return self.update_stats(source, target, STATS_TRAINER)

    # Race
    @import_model(Race, 'numcourse', 'source_id', priority=2)
    def races(self, source, results, target, **kwargs):
        self.wait('caractraps', 'tracks', **kwargs)
        if target is None:
            trap = results['caractraps'].get(source.comp, None)
            track = results['tracks'].get(source.hippo, None)
            target = Race(
                source_id = source.numcourse,
                track = track,
                type = RACE_TYPES_MAP.get(source.typec, None),
            )
        else:
            trap = None

        time = trap and trap.heure
        target.date = datetime(year=source.jour.year, month=source.jour.month,
                               day=source.jour.day, hour=(time and time.hour) or 0,
                               minute=(time and time.minute) or 0,
                               tzinfo=ZoneInfo(self.timezone))
        target.distance = source.dist
        # TODO: TO INT
        target.allocation = to_int(source.cheque)
        target.meteo = trap and METEO_MAP.get(trap.nebulositelibellecourt)
        target.track_state = trap and TRACK_STATE_MAP.get(trap.meteo)
        target.wind = trap and trap.forcevent
        return target

    @import_model(Runner, 'id', 'source_id', priority=3)
    def runners(self, source, results, target, **kwargs):
        self.wait('races', 'horses', **kwargs)
        if target is None:
            race = results['races'].get(source.numcourse)
            horse = results['horses'].get(source.idche)
            target = Runner(
                source_id = source.id,
                race = race,
                horse = horse,
                shoeing = SHOE_MAP.get(source.defoeil),
                unshoeing_first = source.deffirsttime,
                blinder = BLINDER_MAP.get(source.oeil),
                blinder_first = source.oeilfirsttime,
                last_blinder = to_int(source.dernieroeil) or None,
                weight_bred = to_int(source.poidmont),
                track_habit = source.appetterrain,
                first_race = source.indicateurinedit,
                handicap = to_int(source.vha),
                handicap_distance = source.handicapdistance,
                handicap_weight = source.handicappoids,
                redkm = to_msecs(source.redkm),
                supplemented = source.estsupplemente,
                record = to_msecs(source.recordg),
                engagement = source.engagement,
                number = source.numero,
            )
        target.arrival_outdistance = source.ecar
        # TODO: TO INT
        target.gains = to_int(source.gains)

        target.cote_pmu = source.cotedirect
        target.cote_prop = source.coteprob
        target.last_run_age = source.recence
        target.last_allocation = source.dernierealloc
        target.last_cote = source.dernierecote
        return target

