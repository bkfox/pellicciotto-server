import sys
from django.db import models
from django.db.models import Subquery, OuterRef
from django.utils.translation import gettext_lazy as _

from pellicciotto import models as pellicciotto


__all__ = ('CacheDate', 'CaracTrap')


IS_TEST = (len(sys.argv) > 1 and sys.argv[1] == 'test')


class CacheDateQuerySet(models.QuerySet):
    def not_imported(self):
        return self.exclude(id__in = Subquery(
            pellicciotto.Runner.objects.filter(source_id=OuterRef('id'))
                        .values_list('source_id', flat=True)
        ))

class CacheDate(models.Model):
    SEX_CHOICES = (
        ('F', _('Female')),
        ('M', _('Male')),
        ('H', _('Gelding')),
        ('X', _('Unknown')),
    )
    TYPE_CHOICES = (
        ('Attelé', _('Harness')),
        ('Haies', _('Fence')),
        ('Monté', _('Bred')),
        ('Plat', _('Flat')),
        ('Steeple-Chase', _('Steeple-Chase')),
        ('Steeple-Chase Cross-Country', _('Steeple-Chase Cross-Country')),
    )
    
    id = models.BigIntegerField(primary_key=True)
    comp = models.IntegerField()
    jour = models.DateField()
    hippo = models.TextField()
    numcourse = models.BigIntegerField()
    cl = models.TextField()
    dist = models.SmallIntegerField()
    partant = models.IntegerField()
    typec = models.CharField(max_length=27, choices=TYPE_CHOICES)
    cheque = models.CharField(max_length=25)
    numero = models.DecimalField(max_digits=2, decimal_places=0)
    cheval = models.CharField(max_length=255)
    sexe = models.TextField(choices=SEX_CHOICES)
    age = models.IntegerField()
    cotedirect = models.DecimalField(max_digits=5, decimal_places=2)
    coteprob = models.DecimalField(max_digits=5, decimal_places=2)
    recence = models.IntegerField()
    ecurie = models.CharField(max_length=1)
    distpoids = models.TextField(blank=True, null=True)
    ecar = models.CharField(max_length=15)
    redkm = models.CharField(max_length=15)
    redkmint = models.IntegerField(db_column='redkmInt', blank=True, null=True)  # Field name made lowercase.
    corde = models.DecimalField(max_digits=2, decimal_places=0)
    defoeil = models.CharField(max_length=10)
    defoeilprec = models.CharField(db_column='defoeilPrec', max_length=10)  # Field name made lowercase.
    recul = models.IntegerField(blank=True, null=True)
    gains = models.CharField(max_length=14)
    musiquept = models.CharField(max_length=24)
    musiqueche = models.TextField()
    m1 = models.IntegerField(blank=True, null=True)
    m2 = models.IntegerField(blank=True, null=True)
    m3 = models.IntegerField(blank=True, null=True)
    m4 = models.IntegerField(blank=True, null=True)
    m5 = models.IntegerField(blank=True, null=True)
    m6 = models.IntegerField(blank=True, null=True)
    jockey = models.CharField(max_length=33)
    musiquejoc = models.TextField()
    montesdujockeyjour = models.CharField(max_length=2)
    couruejockeyjour = models.CharField(max_length=2)
    victoirejockeyjour = models.CharField(max_length=2)
    entraineur = models.CharField(max_length=33)
    musiqueent = models.TextField()
    monteentraineurjour = models.SmallIntegerField()
    courueentraineurjour = models.SmallIntegerField()
    victoireentraineurjour = models.SmallIntegerField()
    coursescheval = models.SmallIntegerField()
    victoirescheval = models.SmallIntegerField()
    placescheval = models.SmallIntegerField()
    coursesentraineur = models.SmallIntegerField()
    victoiresentraineur = models.SmallIntegerField()
    placeentraineur = models.SmallIntegerField()
    coursesjockey = models.SmallIntegerField()
    victoiresjockey = models.SmallIntegerField()
    placejockey = models.SmallIntegerField()
    dernierhippo = models.CharField(max_length=33, blank=True, null=True)
    dernierealloc = models.CharField(max_length=13, blank=True, null=True)
    derniernbpartants = models.CharField(max_length=2, blank=True, null=True)
    dernieredist = models.CharField(max_length=5, blank=True, null=True)
    derniereplace = models.CharField(max_length=10, blank=True, null=True)
    dernierecote = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    dernierjoc = models.CharField(db_column='dernierJoc', max_length=33, blank=True, null=True)  # lowercase.
    dernierent = models.CharField(db_column='dernierEnt', max_length=33, blank=True, null=True)  # lowercase.
    dernierprop = models.CharField(db_column='dernierProp', max_length=41, blank=True, null=True)  # lowercase.
    dernierredkm = models.CharField(db_column='dernierRedKm', max_length=15)  # lowercase.
    proprietaire = models.CharField(max_length=41)
    nbcoursepropjour = models.IntegerField()
    poidmont = models.CharField(max_length=6)
    pourcvictjock = models.DecimalField(db_column='pourcVictJock', max_digits=5, decimal_places=2)  # lowercase.
    pourcplacejock = models.DecimalField(db_column='pourcPlaceJock', max_digits=5, decimal_places=2)  # lowercase.
    pourcvictcheval = models.DecimalField(db_column='pourcVictCheval', max_digits=5, decimal_places=2)  # lowercase.
    pourcplacecheval = models.DecimalField(db_column='pourcPlaceCheval', max_digits=5, decimal_places=2)  # lowercase.
    pourcvictent = models.DecimalField(db_column='pourcVictEnt', max_digits=5, decimal_places=2)  # lowercase.
    pourcplaceent = models.DecimalField(db_column='pourcPlaceEnt', max_digits=5, decimal_places=2)  # lowercase.
    pourcvictenthippo = models.DecimalField(db_column='pourcVictEntHippo', max_digits=5, decimal_places=2)  # lowercase.
    pourcplaceenthippo = models.DecimalField(db_column='pourcPlaceEntHippo', max_digits=5, decimal_places=2)  # lowercase.
    pourcvictjockhippo = models.DecimalField(db_column='pourcVictJockHippo', max_digits=5, decimal_places=2)  # lowercase.
    pourcplacejockhippo = models.DecimalField(db_column='pourcPlaceJockHippo', max_digits=5, decimal_places=2)  # lowercase.
    pourcvictchevalhippo = models.DecimalField(db_column='pourcVictChevalHippo', max_digits=5, decimal_places=2)  # lowercase.
    pourcplacechevalhippo = models.DecimalField(db_column='pourcPlaceChevalHippo', max_digits=5, decimal_places=2)  # lowercase.
    nbrcoursejockhippo = models.SmallIntegerField(db_column='nbrCourseJockHippo')  # lowercase.
    nbrcourseenthippo = models.SmallIntegerField(db_column='nbrCourseEntHippo')  # lowercase.
    nbrcoursechevalhippo = models.SmallIntegerField(db_column='nbrCourseChevalHippo')  # lowercase.
    nbcoursecouple = models.SmallIntegerField(db_column='nbCourseCouple')  # lowercase.
    nbvictcouple = models.SmallIntegerField(db_column='nbVictCouple')  # lowercase.
    nbplacecouple = models.SmallIntegerField(db_column='nbPlaceCouple')  # lowercase.
    txvictcouple = models.DecimalField(db_column='TxVictCouple', max_digits=5, decimal_places=2)  # lowercase.
    txplacecouple = models.DecimalField(db_column='TxPlaceCouple', max_digits=5, decimal_places=2)  # lowercase.
    nbcoursecouplehippo = models.SmallIntegerField(db_column='nbCourseCoupleHippo')  # lowercase.
    nbvictcouplehippo = models.SmallIntegerField(db_column='nbVictCoupleHippo')  # lowercase.
    nbplacecouplehippo = models.SmallIntegerField(db_column='nbPlaceCoupleHippo')  # lowercase.
    txvictcouplehippo = models.DecimalField(db_column='TxVictCoupleHippo', max_digits=5, decimal_places=2)  # lowercase.
    txplacecouplehippo = models.DecimalField(db_column='TxPlaceCoupleHippo', max_digits=5, decimal_places=2)  # lowercase.
    pere = models.CharField(max_length=25)
    mere = models.CharField(max_length=25)
    peremere = models.CharField(max_length=25)
    commen = models.TextField()
    gainscarriere = models.IntegerField(db_column='gainsCarriere')  # lowercase.
    gainsvictoires = models.IntegerField(db_column='gainsVictoires')  # lowercase.
    gainsplace = models.IntegerField(db_column='gainsPlace')  # lowercase.
    gainsanneeencours = models.IntegerField(db_column='gainsAnneeEnCours')  # lowercase.
    gainsanneeprecedente = models.IntegerField(db_column='gainsAnneePrecedente')  # lowercase.
    jumentpleine = models.IntegerField(db_column='jumentPleine')  # lowercase.
    engagement = models.IntegerField()
    handicapdistance = models.SmallIntegerField(db_column='handicapDistance')  # lowercase.
    handicappoids = models.SmallIntegerField(db_column='handicapPoids')  # lowercase.
    indicateurinedit = models.IntegerField(db_column='indicateurInedit')  # lowercase.
    tempstot = models.CharField(max_length=7)
    vha = models.CharField(max_length=5)
    recordg = models.CharField(db_column='recordG', max_length=8, blank=True, null=True)  # lowercase.
    recordgint = models.IntegerField(db_column='recordGint')  # lowercase.
    txreclam = models.CharField(max_length=5)
    derniertxreclam = models.CharField(db_column='dernierTxreclam', max_length=5)  # lowercase.
    createdat = models.DateTimeField()
    updatedat = models.DateTimeField()
    rangtxvictjock = models.IntegerField(db_column='rangTxVictJock')  # lowercase.
    rangtxvictcheval = models.IntegerField(db_column='rangTxVictCheval')  # lowercase.
    rangtxvictent = models.IntegerField(db_column='rangTxVictEnt')  # lowercase.
    rangtxplacejock = models.IntegerField(db_column='rangTxPlaceJock')  # lowercase.
    rangtxplacecheval = models.IntegerField(db_column='rangTxPlaceCheval')  # lowercase.
    rangtxplaceent = models.IntegerField(db_column='rangTxPlaceEnt')  # lowercase.
    rangrecordg = models.IntegerField(db_column='rangRecordG')  # lowercase.
    appetterrain = models.SmallIntegerField(db_column='appetTerrain')  # lowercase.
    idche = models.IntegerField(db_column='idChe')  # lowercase.
    idjockey = models.IntegerField(db_column='idJockey')  # lowercase.
    identraineur = models.IntegerField(db_column='idEntraineur')  # lowercase.
    deffirsttime = models.IntegerField(db_column='defFirstTime')  # lowercase.
    oeilfirsttime = models.IntegerField(db_column='oeilFirstTime')  # lowercase.
    estsupplemente = models.IntegerField(db_column='estSupplemente')  # lowercase.
    oeil = models.CharField(max_length=1, blank=True, null=True)
    devise = models.TextField()
    dernieroeil = models.CharField(db_column='dernierOeil', max_length=1, blank=True, null=True)  # lowercase.

    objects = CacheDateQuerySet.as_manager()

    class Meta:
        managed = IS_TEST
        db_table = 'cachedate'


class CaracTrap(models.Model):
    id = models.BigIntegerField(primary_key=True)
    comp = models.IntegerField()
    jour = models.CharField(max_length=255, blank=True, null=True)
    heure = models.TimeField(blank=True, null=True)
    hippo = models.CharField(max_length=255, blank=True, null=True)
    reun = models.CharField(max_length=255, blank=True, null=True)
    prix = models.FloatField(blank=True, null=True)
    prixnom = models.CharField(max_length=255, blank=True, null=True)
    meteo = models.CharField(max_length=255, blank=True, null=True)
    typec = models.CharField(max_length=255, blank=True, null=True)
    partant = models.CharField(max_length=255, blank=True, null=True)
    handi = models.CharField(max_length=255, blank=True, null=True)
    reclam = models.CharField(max_length=255, blank=True, null=True)
    dist = models.CharField(max_length=255, blank=True, null=True)
    groupe = models.CharField(max_length=255, blank=True, null=True)
    sex = models.CharField(max_length=255, blank=True, null=True)
    corde = models.CharField(max_length=255, blank=True, null=True)
    age = models.CharField(max_length=255, blank=True, null=True)
    autos = models.CharField(max_length=255, blank=True, null=True)
    cheque = models.CharField(max_length=255, blank=True, null=True)
    europ = models.CharField(max_length=255, blank=True, null=True)
    quinte = models.FloatField(blank=True, null=True)
    natpis = models.CharField(max_length=255, blank=True, null=True)
    amat = models.CharField(max_length=255, blank=True, null=True)
    courseabc = models.CharField(max_length=255, blank=True, null=True)
    pistegp = models.CharField(max_length=255, blank=True, null=True)
    arriv = models.CharField(max_length=255, blank=True, null=True)
    lice = models.CharField(max_length=255, blank=True, null=True)
    temperature = models.IntegerField(blank=True, null=True)
    forcevent = models.IntegerField(db_column='forceVent', blank=True, null=True)  # Field name made lowercase.
    directionvent = models.CharField(db_column='directionVent', max_length=6)  # Field name made lowercase.
    nebulositelibellecourt = models.TextField(db_column='nebulositeLibelleCourt')  # Field name made lowercase.
    condi = models.TextField(blank=True, null=True)
    url = models.TextField()
    tempscourse = models.TextField()
    updatedat = models.DateTimeField(db_column='updatedAt', blank=True, null=True)  # Field name made lowercase.
    createdat = models.DateTimeField(db_column='createdAt', blank=True, null=True)  # Field name made lowercase.
    devise = models.TextField()
    ref = models.TextField()

    class Meta:
        managed = IS_TEST
        db_table = 'caractrap'


